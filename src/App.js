import './App.css';
import './style/landingPage.css'
import NavigationBar from './components/navigasi';
import './components/intro.js'
import Intro from './components/intro.js';
import Trending from './components/trending';
import Petualang from './components/petualang';

function App() {
  return (
    <div>
      {/*Intro section */}
      <div className='myBG border'>
        <NavigationBar />
        <Intro />
        </div>
      {/*end Intro section */}

      <div className='trending'>
        <Trending />
      </div>

      <div className='petualang'>
        <Petualang />
      </div>
    </div>
  );
}

export default App;
