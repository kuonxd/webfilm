import { Col, Container, Row, Button} from 'react-bootstrap'
import '../style/landingPage.css'

const Intro = () =>{
    return(
        <div className='intro'>
            <Container className='text-white d-flex justify-content-center
            align-items-center text-center'>
                <Row>
                    <Col>
                    <div className='title'>Nonton Gratis</div>
                    <div className='title'>Di Mana Saja</div>
                    <div className='introbutton mt-4 text-center'>
                        <Button variant='dark'>Liat Semua List</Button>
                    </div>
                    </Col>
                </Row>
        </Container>
        </div>
    )
}

export default Intro;