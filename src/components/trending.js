import foreigner from "../assets/images/trending/foreigner.jpg"
import joker from "../assets/images/trending/joker.jpg"
import wwz from "../assets/images/trending/world-war-z.jpg"
import yourname from "../assets/images/trending/your-name.jpg"

const Trending = () => {
    return(
        <section class="wrapper">
            <div class="container-fostrap">
                <h1 className="text-white mb-2">Trending Movies</h1>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4" id="trending">
                                <div class="card">
                                    <a class="img-card" href="http://www.fostrap.com/2016/03/bootstrap-3-carousel-fade-effect.html">
                                    <img src={foreigner} />
                                  </a>
                                    <div class="card-content">
                                        <h4 class="card-title">
                                            <a href="http://www.fostrap.com/2016/03/bootstrap-3-carousel-fade-effect.html"> The Foreigner
                                          </a>
                                        </h4>
                                        <p class="">
                                            Tutorial to make a carousel bootstrap by adding more wonderful effect fadein ...
                                        </p>
                                    </div>
                                    <div class="card-read-more">
                                        <a href="http://www.fostrap.com/2016/03/bootstrap-3-carousel-fade-effect.html" class="btn btn-link btn-block">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="card">
                                    <a class="img-card" href="http://www.fostrap.com/2016/03/5-button-hover-animation-effects-css3.html">
                                    <img src={wwz} />
                                  </a>
                                    <div class="card-content">
                                        <h4 class="card-title">
                                             <a href="http://www.fostrap.com/2016/02/awesome-material-design-responsive-menu.html"> World War Z
                                          </a>
                                        </h4>
                                        <p class="">
                                            Material Design is a visual programming language made by Google.
                                        </p>
                                    </div>
                                    <div class="card-read-more">
                                        <a href="https://codepen.io/wisnust10/full/ZWERZK/" class="btn btn-link btn-block">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="card">
                                    <a class="img-card" href="http://www.fostrap.com/2016/03/5-button-hover-animation-effects-css3.html">
                                    <img src={joker} />
                                  </a>
                                    <div class="card-content">
                                        <h4 class="card-title">
                                            <a href="http://www.fostrap.com/2016/03/5-button-hover-animation-effects-css3.html">Joker
                                          </a>
                                        </h4>
                                        <p class="">
                                            tutorials button hover animation, although very much a hover button is very beauti...
                                        </p>
                                    </div>
                                    <div class="card-read-more">
                                        <a href="http://www.fostrap.com/2016/03/5-button-hover-animation-effects-css3.html" class="btn btn-link btn-block">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    )
}

export default Trending;