import hb1 from "../assets/images/petualang/hobbit1.jpg"
import myway from "../assets/images/petualang/myway.jpg"
import entahlah from "../assets/images/petualang/entahlah.jpg"

const Petualang = () => {
    return(
        <section class="wrapper">
            <div class="container-fostrap">
                <h1 className="text-white mb-2">Adventure Movies</h1>
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4" id="petualang">
                                <div class="card">
                                    <a class="img-card" href="http://www.fostrap.com/2016/03/bootstrap-3-carousel-fade-effect.html">
                                    <img src={hb1} />
                                  </a>
                                    <div class="card-content">
                                        <h4 class="card-title">
                                            <a href="http://www.fostrap.com/2016/03/bootstrap-3-carousel-fade-effect.html"> The Hobbit
                                          </a>
                                        </h4>
                                        <p class="">
                                            Tutorial to make a carousel bootstrap by adding more wonderful effect fadein ...
                                        </p>
                                    </div>
                                    <div class="card-read-more">
                                        <a href="http://www.fostrap.com/2016/03/bootstrap-3-carousel-fade-effect.html" class="btn btn-link btn-block">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="card">
                                    <a class="img-card" href="http://www.fostrap.com/2016/03/5-button-hover-animation-effects-css3.html">
                                    <img src={myway} />
                                  </a>
                                    <div class="card-content">
                                        <h4 class="card-title">
                                             <a href="http://www.fostrap.com/2016/02/awesome-material-design-responsive-menu.html"> My Way
                                          </a>
                                        </h4>
                                        <p class="">
                                            Material Design is a visual programming language made by Google.
                                        </p>
                                    </div>
                                    <div class="card-read-more">
                                        <a href="https://codepen.io/wisnust10/full/ZWERZK/" class="btn btn-link btn-block">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="card">
                                    <a class="img-card" href="http://www.fostrap.com/2016/03/5-button-hover-animation-effects-css3.html">
                                    <img src={entahlah} />
                                  </a>
                                    <div class="card-content">
                                        <h4 class="card-title">
                                            <a href="http://www.fostrap.com/2016/03/5-button-hover-animation-effects-css3.html">The Northman
                                          </a>
                                        </h4>
                                        <p class="">
                                            tutorials button hover animation, although very much a hover button is very beauti...
                                        </p>
                                    </div>
                                    <div class="card-read-more">
                                        <a href="http://www.fostrap.com/2016/03/5-button-hover-animation-effects-css3.html" class="btn btn-link btn-block">
                                            Read More
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    )
}

export default Petualang;